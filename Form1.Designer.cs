﻿namespace NoiseCanvas2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.richTextBox_log = new System.Windows.Forms.RichTextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox_cellSize = new System.Windows.Forms.TextBox();
            this.textBox_biomeCellSize = new System.Windows.Forms.TextBox();
            this.textBox_SeedString = new System.Windows.Forms.TextBox();
            this.textBox_PointRandomness = new System.Windows.Forms.TextBox();
            this.checkBox_seedString = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(823, 567);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // richTextBox_log
            // 
            this.richTextBox_log.Location = new System.Drawing.Point(1295, 12);
            this.richTextBox_log.Name = "richTextBox_log";
            this.richTextBox_log.Size = new System.Drawing.Size(244, 529);
            this.richTextBox_log.TabIndex = 1;
            this.richTextBox_log.Text = "";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkBox_seedString);
            this.groupBox1.Controls.Add(this.textBox_PointRandomness);
            this.groupBox1.Controls.Add(this.textBox_SeedString);
            this.groupBox1.Controls.Add(this.textBox_biomeCellSize);
            this.groupBox1.Controls.Add(this.textBox_cellSize);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(1295, 557);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(244, 319);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Controls";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "CellSize:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "BiomeCellSize: ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 107);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "SeedString: ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 144);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "PointRandomness: ";
            // 
            // textBox_cellSize
            // 
            this.textBox_cellSize.Location = new System.Drawing.Point(129, 30);
            this.textBox_cellSize.Name = "textBox_cellSize";
            this.textBox_cellSize.Size = new System.Drawing.Size(109, 20);
            this.textBox_cellSize.TabIndex = 4;
            // 
            // textBox_biomeCellSize
            // 
            this.textBox_biomeCellSize.Location = new System.Drawing.Point(129, 67);
            this.textBox_biomeCellSize.Name = "textBox_biomeCellSize";
            this.textBox_biomeCellSize.Size = new System.Drawing.Size(109, 20);
            this.textBox_biomeCellSize.TabIndex = 5;
            // 
            // textBox_SeedString
            // 
            this.textBox_SeedString.Location = new System.Drawing.Point(129, 104);
            this.textBox_SeedString.Name = "textBox_SeedString";
            this.textBox_SeedString.Size = new System.Drawing.Size(109, 20);
            this.textBox_SeedString.TabIndex = 6;
            // 
            // textBox_PointRandomness
            // 
            this.textBox_PointRandomness.Location = new System.Drawing.Point(129, 141);
            this.textBox_PointRandomness.Name = "textBox_PointRandomness";
            this.textBox_PointRandomness.Size = new System.Drawing.Size(109, 20);
            this.textBox_PointRandomness.TabIndex = 7;
            // 
            // checkBox_seedString
            // 
            this.checkBox_seedString.AutoSize = true;
            this.checkBox_seedString.Location = new System.Drawing.Point(9, 179);
            this.checkBox_seedString.Name = "checkBox_seedString";
            this.checkBox_seedString.Size = new System.Drawing.Size(109, 17);
            this.checkBox_seedString.TabIndex = 8;
            this.checkBox_seedString.Text = "Iterate seedString";
            this.checkBox_seedString.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1557, 888);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.richTextBox_log);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.RichTextBox richTextBox_log;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_PointRandomness;
        private System.Windows.Forms.TextBox textBox_SeedString;
        private System.Windows.Forms.TextBox textBox_biomeCellSize;
        private System.Windows.Forms.TextBox textBox_cellSize;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox checkBox_seedString;
    }
}

