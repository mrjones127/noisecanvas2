﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NoiseCanvas2.Voronoi.SHull;

namespace NoiseCanvas2.Voronoi
{
    class BiomeInfo
    {
        public Biome biome = new Biome();
        public List<int> cellIndexes = new List<int>();
        public int seedCellIndex;
        public int highestIteration;
    }
}
