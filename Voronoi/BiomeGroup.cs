﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NoiseCanvas2.Voronoi.SHull;

namespace NoiseCanvas2.Voronoi
{
    public class BiomeGroup
    {
        public string name;
        public int id = -1;
        public List<Biome> biomes = new List<Biome>();
        //temp, moist  = centroid
        public float temperature;
        public float moisture;
        public int biomeUseCounter;
        public Color color;
        public Vertex mapLocation;
        //biomeGroup, moisture, temperature
        //Tundra -1.0f, -1.0f
        //Desert -1.0f, 1.0f
        //Rainforest 1.0f, 0.8f
        //Boreal 1.0f, -0.5f
        //forest 0.0f, 0.0f
        //all of these groups will end up being relative to the biomes supplied. Im taking the best fitting ones not the ones that match
        public BiomeGroup(string name, float temperature, float moisture, Color color)
        {
            this.name = name;
            this.moisture = moisture;
            this.temperature = temperature;
            this.color = color;
        }
    }
}
