﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NoiseCanvas2.Voronoi.SHull;

namespace NoiseCanvas2.Voronoi
{
    public class Cell
    {
        public Vertex position = new Vertex(0,0);
        public List<int> adjacencies = new List<int>();
        public List<Vertex> vertices = new List<Vertex>();
        public Biome biome = new Biome();
        public int iterationIndex;
        public bool isAssigned;
        public int assignAttempts;
        public int parentIndex;
        public int index;
        public int childIndex;
        public bool isBorder;
        public int biomeGroupIndex;
        public Cell(Vertex position, List<int> adjacencies, List<Vertex> vertices, float temperature, Biome biome)
        {
            this.position = position;
            this.adjacencies = adjacencies;
            this.vertices = vertices;
            this.biome = biome;
        }

        public Cell()
        {
            
        }
    }
}
