﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using NoiseCanvas2.PRNG;
using NoiseCanvas2.Voronoi.SHull;

namespace NoiseCanvas2.Voronoi
{
    class VoronoiFunctions
    {

        public T[] wellsort<T>(T[] inputarray, string seedString)
        {
            Well19937c Well = new Well19937c(seedString.GetHashCode());
            Vertex[] sortedarray = new Vertex[inputarray.Length];
            Parallel.For(0, sortedarray.Length, i =>
            {
                sortedarray[i] = new Vertex(0, 0);
            });
            Parallel.For(0, inputarray.Length, i =>
            {
                sortedarray[i].x = i;
                sortedarray[i].y = Well.nextFloat();
            });
            sortedarray = sortedarray.OrderBy(v => v.y).ToArray();
            T[] returnlist = new T[inputarray.Length];
            for (int i = 0; i < inputarray.Length; i++)
                returnlist[i] = inputarray[(int)sortedarray[i].x];
            return returnlist;
        }
        public List<T> wellsort<T>(List<T> inputarray, string seedString)
        {
            Well19937c Well = new Well19937c(seedString.GetHashCode());
            Vertex[] sortedarray = new Vertex[inputarray.Count];
            Parallel.For(0, sortedarray.Length, i =>
            {
                sortedarray[i] = new Vertex(0, 0);
            });
            Parallel.For(0, inputarray.Count, i =>
            {
                sortedarray[i].x = i;
                sortedarray[i].y = Well.nextFloat();
            });
            sortedarray = sortedarray.OrderBy(v => v.y).ToArray();
            List<T> returnlist = new List<T>();
            for (int i = 0; i < inputarray.Count; i++)
                returnlist.Add(inputarray[(int)sortedarray[i].x]);
            return returnlist;
        }

        public int findapproximate(Vertex v, int cellSize)
        {
            //int w = fastfloor(v.x/cellSize);//cells to the right
            //int h = fastfloor(v.y/cellSize);//cells down
            //int index = w*h;
            return fastfloor(v.x * 1.0000 / cellSize) * fastfloor(v.y * 1.0000 / cellSize);
        }
        public int fastfloor(double x)
        {
            return x > 0 ? (int)x : (int)x - 1;
        }
        public Cell[] sortCells(Cell[] cells, int cellSize)
        {
            return cells.OrderBy(v => v.position.x + Math.Round(v.position.y * 1.00000 / 20) * 20).ToArray();
        }

        public int findCenterCell(Cell[] cells, int mapwidth, int mapheight)
        {
            mapwidth = mapwidth / 2;
            mapheight = mapheight / 2;
            int cellindex = 0;
            float distance = 99999999.000000f;
            for (int index = 0; index < cells[cellindex].adjacencies.Count; index++)
            {
                int t = cells[cellindex].adjacencies[index];
                float temp = (cells[t].position.x - mapwidth) * (cells[t].position.x - mapwidth) +
                             (cells[t].position.y - mapheight) * (cells[t].position.y - mapheight);
                if (!(temp < distance)) continue;
                distance = temp;
                cellindex = t;
            }
            return cellindex;
        }

        public int findnearestwapproximate(Vertex vertex, Cell[] cells, int approximateIndex)
        {
            if (approximateIndex < 0 || approximateIndex > cells.Length)
                approximateIndex = 0;
            int cellindex = approximateIndex;
            float distance = 99999999.000000f;
            bool done = false;
            while (!done)
            {
                done = true;
                for (int index = 0; index < cells[cellindex].adjacencies.Count; index++)
                {
                    int t = cells[cellindex].adjacencies[index];
                    float temp = (cells[t].position.x - vertex.x) * (cells[t].position.x - vertex.x) +
                                  (cells[t].position.y - vertex.y) * (cells[t].position.y - vertex.y);
                    if (!(temp < distance)) continue;
                    distance = temp;
                    cellindex = t;
                    done = false;
                    break;
                }
            }
            return cellindex;
        }

        public int findnearestborder(int startCellIndex, Cell[] cells, Vertex destination)
        {
            int cellindex = startCellIndex;
            float distance = 99999999.00f;
            bool done = false;
            while (!done)
            {
                done = true;
                for (int index = 0; index < cells[cellindex].adjacencies.Count; index++)
                {
                    int newcell = cells[cellindex].adjacencies[index];
                    float tempDistance = (cells[newcell].position.x - cells[startCellIndex].position.x) * (cells[newcell].position.x - cells[startCellIndex].position.x) +
                                         (cells[newcell].position.y - cells[startCellIndex].position.y) * (cells[newcell].position.y - cells[startCellIndex].position.y);
                    if (!(tempDistance < distance)) continue;
                    distance = tempDistance;
                    cellindex = newcell;
                    done = false;
                    break;
                }
                if (cells[cellindex].biome != cells[startCellIndex].biome)
                    done = true;
            }
            return cellindex;
        }
        public int findnearest(Vertex vertex, Cell[] cells, int centerCellIndex)
        {
            int cellindex = centerCellIndex;
            float distance = 99999999.00f;
            bool done = false;
            while (!done)
            {
                done = true;
                for (int index = 0; index < cells[cellindex].adjacencies.Count; index++)
                {
                    int newcell = cells[cellindex].adjacencies[index];
                    float tempDistance = (cells[newcell].position.x - vertex.x) * (cells[newcell].position.x - vertex.x) +
                                         (cells[newcell].position.y - vertex.y) * (cells[newcell].position.y - vertex.y);
                    if (!(tempDistance < distance)) continue;
                    distance = tempDistance;
                    cellindex = newcell;
                    done = false;
                    break;
                }
            }
            return cellindex;
        }

        public int forcefindnearest(Vertex start, Vertex[] destinations)
        {
            float distance = 9999999999f;
            int nearestIndex = 0;
            for (int i = 0; i < destinations.Length; i++)
            {
                float tempDistance = (destinations[i].x - start.x) * (destinations[i].x - start.x) +
                                         (destinations[i].y - start.y) * (destinations[i].y - start.y);
                if (!(tempDistance < distance)) continue;
                distance = tempDistance;
                nearestIndex = i;
            }
            return nearestIndex;
        }
        ////to use this invert the coordinates of what you would get by running findapproximate()
        //public int findfurthest(Vertex vertex, Cell[] cells)
        //{
        //    int cellindex = 0;
        //    float distance = 0.00f;
        //    bool done = false;
        //    while (!done)
        //    {
        //        done = true;
        //        if (cells[cellindex].adjacencies.Count < 3)
        //        {
        //            cellindex++;
        //        }
        //        for (int index = 0; index < cells[cellindex].adjacencies.Count; index++)
        //        {
        //            int t = cells[cellindex].adjacencies[index];
        //            float temp = (cells[t].position.x - vertex.x) * (cells[t].position.x - vertex.x) +
        //                          (cells[t].position.y - vertex.x) * (cells[t].position.y - vertex.x);
        //            if (!(temp > distance)) continue;
        //            distance = temp;
        //            cellindex = t;
        //            done = false;
        //            break;
        //        }
        //    }
        //    return cellindex;
        //}
    }
}
