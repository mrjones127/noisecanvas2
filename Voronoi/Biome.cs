﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NoiseCanvas2.Voronoi.SHull;

namespace NoiseCanvas2.Voronoi
{
    public class Biome
    {
        public string name = "Null";
        public float temperature = 0.0f;
        public float moisture = 0.0f;
        public string[] cantBorderName = new string[0];
        public float[] cantBorderTemperature = new float[0];
        public Color color = Color.White;
        public BiomeType biomeType = BiomeType.primary;
        public string growthFunction = "(x)^3";
        //growth factor is what determines the percent chance that during the growth phase of the biome that a cell will be grown into that biome in an iteration
        public float growthFactor;
        public int id = -1;
        public int biomeGroupId = -1;
        //this is where the biome spawned
        public Vertex spawnVertex;
        public enum BiomeType
        {
            primary,
            connecting,
            sub,

        }

        //when the biome is a sub biome use the growthFactor as a sort of "its gonna be around this size when its done" instead of growthFactor
        public Biome(string name, float temperature, float moisture, string[] cantBorderName, float[] cantBorderTemperature, Color color, BiomeType biomeType, float growthFactor)
        {
            this.name = name;
            this.temperature = temperature;
            this.moisture = moisture;
            this.cantBorderName = cantBorderName;
            this.cantBorderTemperature = cantBorderTemperature;
            this.biomeType = biomeType;
            this.color = color;
            this.growthFactor = (float) (growthFactor * 1.00000 / 31);
        }

        public Biome()
        {
            
        }
        //name is the refferable string value of the biome
        //cantbordername will check if adjacent biomes contain matching names
    }
}
