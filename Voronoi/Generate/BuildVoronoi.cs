﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.Serialization.Formatters;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NoiseCanvas2.PRNG;
using NoiseCanvas2.Voronoi.SHull;

/*
 * Notes
 * ListCell and Cell exist because ListCell is for adding stuff too during triangulation because its faster
 * and Cell exists because arrays are faster than Lists
 * 
 * 
 * 
 * 
 */
namespace NoiseCanvas2.Voronoi.Generate
{
    class BuildVoronoi
    {
        Logger logger = new Logger();
        //make these globally available so they can especially be used in debugging
        public Vertex[] global_vertices;
        public Cell[] global_cells;
        public Cell[] generate(int mapwidth, int mapheight, int cellsize, string seedstring, float pointRandomness)
        {
            //point_randomness is the offset multiplier used when equidistributing the points
            //well generates between 1 and 31 so divide the multiplier by 31 to get accurate results
            pointRandomness = (float)(pointRandomness * 1.00000 / 31);

            //distribute points
            global_vertices = distributePoints(mapwidth, mapheight, cellsize, seedstring, pointRandomness);
            global_cells = Triangulate(global_vertices);
            //triangulate points
            return global_cells;
        }

        public Cell[] Triangulate(Vertex[] vertices)
        {
            logger.log("Triangulating Vertices");
            //Note: Llyod relaxations are not needed because it can essentially be controlled through the well offset
            Triangulator angulator = new Triangulator();
            Cell[] cells = new Cell[vertices.Length];
            //initialize the array with values because the values contain lists
            for (int j = 0; j < cells.Length; j++)
                cells[j] = new Cell();
            Triad[] triangles = angulator.Triangulation(vertices.ToList(), true).ToArray();
            int erroCount = 0;
            int index = 0;
            //Task[] tasks = new Task[Environment.ProcessorCount];
            //for (int i = 0; i < Environment.ProcessorCount; i++)
            //{
            //    int i1 = i;
            //    tasks[i] = Task.Factory.StartNew(() =>
            //    {
            //        int i2 = i1;
            //        int indexStart = voronoiFunctions.fastfloor(triangles.Length * 1.0000 / Environment.ProcessorCount) * i2;
            //        int indexEnd;
            //        if (i2 != Environment.ProcessorCount - 1)
            //            indexEnd = voronoiFunctions.fastfloor(triangles.Length * 1.0000 / Environment.ProcessorCount) * (i2 + 1);
            //        else
            //            indexEnd = triangles.Length;
            //        for (int j = indexStart; j < indexEnd; j++)
            //        {
            //            cells[triangles[j].a].position = new Vertex(vertices[triangles[j].a].x, vertices[triangles[j].a].y);
            //            cells[triangles[j].b].position = new Vertex(vertices[triangles[j].b].x, vertices[triangles[j].b].y);
            //            cells[triangles[j].a].adjacencies.Add(triangles[j].b);
            //            cells[triangles[j].a].adjacencies.Add(triangles[j].c);
            //            cells[triangles[j].b].adjacencies.Add(triangles[j].a);
            //            cells[triangles[j].b].adjacencies.Add(triangles[j].c);
            //            cells[triangles[j].c].adjacencies.Add(triangles[j].a);
            //            cells[triangles[j].c].adjacencies.Add(triangles[j].b);
            //            cells[triangles[j].a].vertices.Add(new Vertex(triangles[j].circumcircleX, triangles[j].circumcircleY));
            //            cells[triangles[j].b].vertices.Add(new Vertex(triangles[j].circumcircleX, triangles[j].circumcircleY));
            //            cells[triangles[j].c].vertices.Add(new Vertex(triangles[j].circumcircleX, triangles[j].circumcircleY));
            //        }
            //    });
            //}
            //Task.WaitAll(tasks);
            for (int i = 0; i < triangles.Length; i++)
            {
                cells[triangles[i].a].index = triangles[i].a;
                cells[triangles[i].b].index = triangles[i].b;
                cells[triangles[i].c].index = triangles[i].c;
                cells[triangles[i].a].position = new Vertex(vertices[triangles[i].a].x, vertices[triangles[i].a].y);
                cells[triangles[i].b].position = new Vertex(vertices[triangles[i].b].x, vertices[triangles[i].b].y);
                cells[triangles[i].c].position = new Vertex(vertices[triangles[i].c].x, vertices[triangles[i].c].y);
                cells[triangles[i].a].adjacencies.Add(triangles[i].b);
                cells[triangles[i].a].adjacencies.Add(triangles[i].c);
                cells[triangles[i].b].adjacencies.Add(triangles[i].a);
                cells[triangles[i].b].adjacencies.Add(triangles[i].c);
                cells[triangles[i].c].adjacencies.Add(triangles[i].a);
                cells[triangles[i].c].adjacencies.Add(triangles[i].b);
                cells[triangles[i].a].vertices.Add(new Vertex(triangles[i].circumcircleX, triangles[i].circumcircleY));
                cells[triangles[i].b].vertices.Add(new Vertex(triangles[i].circumcircleX, triangles[i].circumcircleY));
                cells[triangles[i].c].vertices.Add(new Vertex(triangles[i].circumcircleX, triangles[i].circumcircleY));
                //index++;
                //if (index % 1000 == 0)
                //    logger.log("Voronoied " + index + " out of " + triangles.Length);
            }
            logger.log("triangulation error count: " + erroCount);
            //remove duplicate adjacencies
            for (int i = 0; i < cells.Length; i++)
            {
                cells[i].adjacencies = cells[i].adjacencies.Distinct().ToList();
                cells[i].vertices = cells[i].vertices.Distinct().ToList();
                cells[i].vertices = cells[i].vertices.OrderByDescending(x => Math.Atan2(x.x, x.y)).ToList();
            }
            return cells;
        }

        public Vertex[] distributePoints(int mapwidth, int mapheight, int cellsize, string seedstring, float pointRandomness)
        {
            logger.log("Dispersing cells");
            //equidistribute all the points and apply offsets at the same time using well
            Well19937c Well = new Well19937c(seedstring.GetHashCode());
            List<Vertex> vertices = new List<Vertex>();
            int index = 0;
            for (int i = 0; i < mapwidth; i += cellsize)
            {
                for (int j = 0; j < mapheight; j += cellsize)
                {
                    //if the vertex is not within the constraints of the map then "undo" the iteration. the well nextfloats will take care of assigning
                    //the vertex to something different
                    Vertex v = new Vertex(i + pointRandomness*Well.nextFloat(), j + pointRandomness*Well.nextFloat());
                    if (v.x > mapwidth || v.x < 0 || v.y > mapheight || v.y < 0)
                    {
                        j -= cellsize;
                        continue;
                    }
                    vertices.Add(v);
                    index++;
                }
            }
            return vertices.ToArray();
        }
        public Bitmap distributPointsToBitmap(int mapwidth, int mapheight, Vertex[] vertices)
        {
            Bitmap b = new Bitmap(mapwidth, mapheight);
            for (int i = 0; i < vertices.Length; i++)
            {
                for (int j = -1; j < 1; j++)
                    for (int k = -1; k < 1; k++)
                    {
                        if (vertices[i].x + j > mapwidth || vertices[i].y + k > mapheight || vertices[i].x + j < 0 || vertices[i].y + k < 0) break;
                        b.SetPixel(fastfloor(vertices[i].x) + j, fastfloor(vertices[i].y) + k, Color.Black);
                    }
            }
            return b;
        }

        private static int fastfloor(double x)
        {
            return x > 0 ? (int)x : (int)x - 1;
        }
    }





}
