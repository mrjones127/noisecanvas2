﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using NoiseCanvas2.PRNG;
using NoiseCanvas2.Voronoi.SHull;

namespace NoiseCanvas2.Voronoi.Generate
{
    class BuildBiomes
    {
        //MAKE
        //MAKE THE ID OF THE BIOME BE ITS SEED CELL INDEX
        Well19937c Well = new Well19937c();
        BuildVoronoi buildVoronoi = new BuildVoronoi();
        private VoronoiFunctions voronoiFunctions = new VoronoiFunctions();
        Logger logger = new Logger();
        public Cell[] generate(Cell[] cells, Biome[] primaryBiomes, BiomeGroup[] biomeGroups, int mapwidth, int mapheight, int biomeCellSize, string seedString, float pointRandomness, int cellSize)
        {
            Tuple<int[], Cell[]> seedReturn = generateBiomeSeeds(cells, primaryBiomes.ToList(), biomeGroups.ToList(), mapwidth, mapheight, biomeCellSize, seedString, pointRandomness);
            int[] seedCellIndexes = seedReturn.Item1;
            cells = seedReturn.Item2;
            Tuple<BiomeInfo[], Cell[]> assignandgrowPrimaryReturn = GrowPrimary(cells, seedCellIndexes, biomeCellSize, cellSize, seedString);
            return assignandgrowPrimaryReturn.Item2;
        }

        private Tuple<int[], Cell[]> generateBiomeSeeds(Cell[] cells, List<Biome> biomes, List<BiomeGroup> biomeGroups, int mapwidth, int mapheight, int biomeCellSize, string seedString, float pointRandomness)
        {
            Bitmap whittakerDiagram = (Bitmap) Image.FromFile("whitfull.png");
            for (int i = 0; i < biomeGroups.Count; i++)
                biomeGroups[i].id = i;
            for (int i = 0; i < biomes.Count; i++)
            {
                //* 1000 converts the 0-1 float into a pixel value
                Color biomeColor = whittakerDiagram.GetPixel((int) biomes[i].temperature, (int) biomes[i].moisture);
                int searchIndex = 0;
                //radial search
                while (biomeGroups.All(group => group.color != biomeColor))
                {
                    for (int j = 0; j < 360; j++)
                    {
                        Color tempColor = whittakerDiagram.GetPixel((int) ((int) biomes[i].temperature + searchIndex*Math.Cos(j)), (int) ((int) biomes[i].moisture + searchIndex * Math.Sin(j)));
                        if (biomeGroups.Any(group => group.color == tempColor))
                        {
                            biomeColor = tempColor;
                            break;
                        }
                    }
                }
                biomes[i].id = i;
                int biomeGroupIndex = biomeGroups.FindIndex(group => group.color == biomes[i].color);
                biomes[i].biomeGroupId = biomeGroups[biomeGroupIndex].id;
                biomeGroups[biomeGroupIndex].biomes.Add(biomes[i]);
            }
            for (int i = 0; i < biomeGroups.Count; i++)
            {
                //sorts biomes by their distance to the biomeGroup's centroid
                biomeGroups[i].biomes =
                    biomeGroups[i].biomes.OrderBy(biome => (biome.temperature - biomeGroups[i].temperature)*(biome.temperature - biomeGroups[i].moisture) + (biome.moisture - biomeGroups[i].moisture)*(biome.moisture - biomeGroups[i].moisture))
                        .ToList();
            }
            //for (int i = 0; i < biomeGroups.Count; i++)
            //{
            //    List<List<Biome>> biomeSplitList = new List<List<Biome>>();
            //    int splitConstant = 2;
            //    //this first part, where its all manual, is for the primary biome. make sure to remove the primary biome otherwise it will be included in the split list splitting
            //    biomeSplitList.Add(new List<Biome>());
            //    biomeSplitList[0].Add(biomeGroups[i].biomes[0]);
            //    biomeGroups[i].biomes.RemoveAt(0);
            //    for (int j = 0; j < splitConstant; j++)
            //    {
            //        biomeSplitList.Add(new List<Biome>());
            //        //if j < length, split between values rounding etc. if not, the end is the length of the array, to account for flooring
            //        if (j != splitConstant - 1)
            //            SubArray(biomeGroups[i].biomes.ToArray(), fastfloor(biomeGroups[i].biomes.Count*1.0000/splitConstant)*j, fastfloor(biomeGroups[i].biomes.Count*1.0000/splitConstant)*(j + 1));
            //        else SubArray(biomeGroups[i].biomes.ToArray(), fastfloor(biomeGroups[i].biomes.Count * 1.0000 / splitConstant) * j, biomeGroups[i].biomes.Count);
            //    }
            //    //clear the list to ready it for the sorted list parts
            //    biomeGroups[i].biomes = new List<Biome>();
            //    //randomly sort each split list, and then add it back to the biome list
            //    for (int j = 0; j < biomeSplitList.Count; j++)
            //        biomeGroups[i].biomes.AddRange(voronoiFunctions.wellsort(biomeSplitList[j], seedString));
            //}
            //sort biome groups by their distance from the origin
            biomeGroups = biomeGroups.OrderBy(group => (group.temperature)*(group.temperature) + (group.moisture)*(group.moisture)).ToList();

            //distribute the biomeGroup points
            Vertex[] biomeGroupSeeds = buildVoronoi.distributePoints(mapwidth, mapheight, (mapwidth*mapheight)/biomeGroups.Count, seedString, 0);
            biomeGroupSeeds = new[]
            {
                new Vertex(0, 0), new Vertex(0, mapheight), new Vertex(mapwidth, 0), new Vertex(mapwidth, mapheight), new Vertex(mapwidth/2, mapheight/2), new Vertex(mapwidth/2, 0), new Vertex(mapwidth/3, mapheight/3),
                new Vertex(0, mapheight/2), new Vertex(mapwidth/2, mapheight)
            };
            //choose starting quadrant for biomegroup propogation
            int quadrantStart = voronoiFunctions.wellsort(new[] { 1, 2, 3, 4 }, seedString)[0];
            int seedStartX = 0;
            int seedStartY = 0;
            switch (quadrantStart)
            {
                case 2:
                    seedStartX = mapwidth;
                    break;
                case 3:
                    seedStartX = mapwidth;
                    seedStartY = mapheight;
                    break;
                case 4:
                    seedStartY = mapheight;
                    break;
            }
            //sort seeds by distance from starting quadrant
            biomeGroupSeeds = biomeGroupSeeds.OrderBy(seed => (seed.x - seedStartX)* (seed.x - seedStartX) + (seed.y - seedStartY) * (seed.y - seedStartY)).ToArray();
            //assign nearest to nearest
            for (int i = 0; i < biomeGroups.Count; i++)
                biomeGroups[i].mapLocation = biomeGroupSeeds[i];

            //distribute biome seeds
            List<Vertex> biomeSeeds = buildVoronoi.distributePoints(mapwidth, mapheight, (mapwidth + mapheight) / biomes.Count, seedString, 0).ToList();

            //this checks out good
            //int what = 0;
            //for (int i = 0; i < biomeGroups.Count; i++)
            //    what += biomeGroups[i].biomes.Count;

            int AssignmentIndex = 0;
            while (AssignmentIndex < biomes.Count)
            {
                for (int i = 0; i < biomeGroups.Count; i++)
                {
                    if (biomeGroups[i].biomes.Count < 1) continue;
                    //finds the next biomeSeed which is closest to the centroid
                    //takes the next biome which is closest to the centroid
                    //assigns the biome spawn location to that point
                    AssignmentIndex++;
                    int spawnPoint = voronoiFunctions.forcefindnearest(biomeGroups[i].mapLocation, biomeSeeds.ToArray());
                    int biomeRefIndex = biomes.FindIndex(biome => biome.id == biomeGroups[i].biomes[0].id);
                    biomes[biomeRefIndex].spawnVertex = biomeSeeds[spawnPoint];
                    biomeSeeds.RemoveAt(spawnPoint);
                    biomeGroups[i].biomes.RemoveAt(0);
                }
            }

            //assign the biomes to their respective cells and record indexes for use in growth function
            int centerCellIndex = voronoiFunctions.findCenterCell(cells, mapwidth, mapheight);
            int[] seedCellIndexes = new int[biomes.Count];
            for (int i = 0; i < biomes.Count; i++)
            {
                int index = voronoiFunctions.findnearest(biomes[i].spawnVertex, cells, centerCellIndex);
                cells[index].biome = biomes[i];
                seedCellIndexes[i] = index;
            }
            return new Tuple<int[], Cell[]>(seedCellIndexes, cells);
        }
        public int fastfloor(double x)
        {
            return x > 0 ? (int)x : (int)x - 1;
        }
        public static T[] SubArray<T>(T[] data, int index, int length)
        {
            if (data.Length < 1) return data;
            if (length >= data.Length)
                length = data.Length - 1;
            T[] result = new T[length];
            Array.Copy(data, index, result, 0, length);
            return result;
        }
        //this method is for primary biomes only
        private Tuple<BiomeInfo[], Cell[]> GrowPrimary(Cell[] cells, int[] seedCellIndexes, int biomeCellSize, int cellSize, string seedString)
        {
            Well = new Well19937c(seedString.GetHashCode());
            BiomeInfo[] biomeInfoArray = new BiomeInfo[seedCellIndexes.Length];
            for (int i = 0; i < biomeInfoArray.Length; i++)
                biomeInfoArray[i] = new BiomeInfo();
            List<int> newlyAssignedCells = seedCellIndexes.ToList();
            List<int> newlyAssignedCells_temp = new List<int>();
            //while there are still cells that havent grown themselves yet, keep iterating
            int biomeloopindex = 0;
            //while (assignedCells.Count < cells.Length)
            bool finishedGenerating = false;
            while (!finishedGenerating)
            {
                finishedGenerating = true;
                while (newlyAssignedCells.Count > 0) //cells.Any(c => c.biome.name == "Null"))
                {
                    //if (biomeloopindex++ % 5 == 0)
                    //    log.log("Biome Growth Iteration: " + biomeloopindex);
                    //for ever cell that was assigned last iteration
                    for (int i = 0; i < newlyAssignedCells.Count; i++)
                    {
                        if (cells[newlyAssignedCells[i]].biome.id == -1 || cells[newlyAssignedCells[i]].biome.name == "Null") continue;
                        //iterate through its adjacent cells
                        for (int j = 0; j < cells[newlyAssignedCells[i]].adjacencies.Count; j++)
                        {
                            if (cells[cells[newlyAssignedCells[i]].adjacencies[j]].biome.id != -1)
                            {
                                if (cells[cells[newlyAssignedCells[i]].adjacencies[j]].biome.id != cells[newlyAssignedCells[i]].biome.id)
                                {
                                    cells[cells[newlyAssignedCells[i]].adjacencies[j]].isBorder = true;
                                    cells[newlyAssignedCells[i]].isBorder = true;
                                }
                                continue;
                            }
                            cells[cells[newlyAssignedCells[i]].adjacencies[j]].assignAttempts++;
                            //if the number of assignmnet attempts is greater than the amount of adjacencies the cell has (minus a side so it can still be generated), then run the randomness statement.
                            if (cells[cells[newlyAssignedCells[i]].adjacencies[j]].assignAttempts < cells[cells[newlyAssignedCells[i]].adjacencies[j]].adjacencies.Count - 2)
                                //and if the randomness gods approve
                                if (Well.nextFloat()*cells[newlyAssignedCells[i]].biome.growthFactor < 0.5f) continue;
                            //add the cell to the biomeinfo, so we can keep track of it
                            biomeInfoArray[cells[newlyAssignedCells[i]].biome.id].cellIndexes.Add(cells[newlyAssignedCells[i]].adjacencies[j]);
                            //give the cell its iteration index, based off of the parent cell's iteration index + 1
                            cells[cells[newlyAssignedCells[i]].adjacencies[j]].iterationIndex = cells[newlyAssignedCells[i]].iterationIndex + 1;
                            //if the iteration index is higher than the highest for that biome, replace it
                            if (cells[cells[newlyAssignedCells[i]].adjacencies[j]].iterationIndex > biomeInfoArray[cells[newlyAssignedCells[i]].biome.id].highestIteration)
                                biomeInfoArray[cells[newlyAssignedCells[i]].biome.id].highestIteration = cells[cells[newlyAssignedCells[i]].adjacencies[j]].iterationIndex;
                            //assign the biome
                            cells[cells[newlyAssignedCells[i]].adjacencies[j]].biome = cells[newlyAssignedCells[i]].biome;
                            //add the newly assigned cell to the temp array
                            newlyAssignedCells_temp.Add(cells[newlyAssignedCells[i]].adjacencies[j]);
                        }
                    }
                    newlyAssignedCells = newlyAssignedCells_temp;
                    newlyAssignedCells_temp = new List<int>();
                }
                for (int i = 0; i < biomeInfoArray.Length; i++)
                {
                    if (biomeInfoArray[i].cellIndexes.Count > 0 && biomeInfoArray[i].cellIndexes.Count < (biomeCellSize/cellSize)/2)
                    {
                        //finishedGenerating = false;
                        for (int j = 0; j < biomeInfoArray[i].cellIndexes.Count; j++)
                            if (cells[biomeInfoArray[i].cellIndexes[j]].isBorder)
                                newlyAssignedCells.Add(biomeInfoArray[i].cellIndexes[j]);
                        for (int j = 0; j < biomeInfoArray[i].cellIndexes.Count; j++)
                        {
                            cells[biomeInfoArray[i].cellIndexes[j]].biome = new Biome();
                            cells[biomeInfoArray[i].cellIndexes[j]].isAssigned = false;
                            cells[biomeInfoArray[i].cellIndexes[j]].assignAttempts = 0;
                        }
                    }
                }
            }

            logger.log("Border distances complete");
            logger.log("Biome Growth Complete.");
            return new Tuple<BiomeInfo[], Cell[]>(biomeInfoArray.ToArray(), cells.ToArray());
        }

        //seedCellIndexes are the same as those for the primary biomes
        //im oging to have to add a lot to this to integrate temperature

    }
}
