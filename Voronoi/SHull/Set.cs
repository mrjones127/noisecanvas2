﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace NoiseCanvas2.Voronoi.SHull
{
    internal class Set<T> : IEnumerable<T>
    {
        private readonly SortedList<T, int> list;

        public Set()
        {
            list = new SortedList<T, int>();
        }

        public int Count => list.Count;

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return list.Keys.GetEnumerator();
        }

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }

        #endregion

        public void Add(T k)
        {
            if (!list.ContainsKey(k))
                list.Add(k, 0);
        }

        public void DeepCopy(Set<T> other)
        {
            list.Clear();
            foreach (T k in other.list.Keys)
                Add(k);
        }

        public void Clear()
        {
            list.Clear();
        }
    }
}
