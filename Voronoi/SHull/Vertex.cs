﻿using System;

namespace NoiseCanvas2.Voronoi.SHull
{
    public class Vertex
    {
        public float x;
        public float y;

        public Vertex()
        {
        }

        public Vertex(float x, float y)
        {
            this.x = x;
            this.y = y;
        }

        public float distance2To(Vertex other)
        {
            float dx = x - other.x;
            float dy = y - other.y;
            return dx * dx + dy * dy;
        }

        public float distanceTo(Vertex other)
        {
            return (float)Math.Sqrt(distance2To(other));
        }

        public override string ToString()
        {
            return $"({x},{y})";
        }
    }
}
