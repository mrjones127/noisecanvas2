﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NoiseCanvas2.Voronoi.SHull;

namespace NoiseCanvas2.Voronoi
{
    class Lloyd
    {
        public Vertex[] relax(Cell[] cells)
        {
            List<Vertex> points = new List<Vertex>();
            Parallel.For(0, cells.Length, i =>
            {
                if (cells[i].vertices.Count <= 0) return;
                int index = 0;
                float xadd = 0;
                float yadd = 0;
                for (int j = 0; j < cells[i].vertices.Count; j++)
                {
                    index++;
                    xadd += cells[i].vertices[j].x;
                    yadd += cells[i].vertices[j].y;
                }
                lock (points)
                    points.Add(new Vertex(xadd / index, yadd / index));
            });
            return points.ToArray();
        }
    }
}
