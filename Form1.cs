﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using NoiseCanvas2.Voronoi;
using NoiseCanvas2.Voronoi.Generate;
using System.Windows.Media;
using Color = System.Windows.Media.Color;

namespace NoiseCanvas2
{
    public partial class Form1 : Form
    {
        //if the cell has only one neighboring cell where the biome is equal to its assigned biome
        //then the biome value of the cell will be set equal to the biome with the highest count of adjacent cells
        //TODO
        //ocean edges
        private static Form1 frm;
        public Form1()
        {
            
            InitializeComponent();
            frm = this;
            Size = new Size(1600, 900);
            pictureBox1.Size = new Size(1300, 800);
            pictureBox1.Location = new Point(-1,-1);
            textBox_cellSize.Text = cellSize.ToString();
            textBox_biomeCellSize.Text = biomeCellSize.ToString();
            textBox_SeedString.Text = seedString;
            textBox_PointRandomness.Text = pointRandomness.ToString();
        }

        private BiomeGroup[] primaryBiomeGroups =
        {
            //    new BiomeGroup("Winter", -0.2f, -1.0f),
            //    new BiomeGroup("Autumn", -0.2f, -0.5f),
            //    new BiomeGroup("Spring", 0.2f, 0.0f),
            //    new BiomeGroup("Summer", 0.1f, 1.0f),
            //    new BiomeGroup("Airid", -0.5f, 0.0f),
            //    new BiomeGroup("Swamp", 0.9f, 0.4f), 
            new BiomeGroup("Rainforest", 865f, 285f, ColorTranslator.FromHtml("#014A2B")),
            new BiomeGroup("Savanna", 870f, 639f, ColorTranslator.FromHtml("#6BA127")),
            new BiomeGroup("Desert", 862f, 917f, ColorTranslator.FromHtml("#905032")),
            new BiomeGroup("Grassland", 509f, 946f, ColorTranslator.FromHtml("#C1A700")),
            new BiomeGroup("SparseWoods", 616f, 816f, ColorTranslator.FromHtml("#982B26")),
            new BiomeGroup("Forest", 582f, 627f, ColorTranslator.FromHtml("#00466A")),
            new BiomeGroup("TemperateRainforest", 648f, 422f, ColorTranslator.FromHtml("#2A6682")),
            new BiomeGroup("BorealForest", 311f, 780f, ColorTranslator.FromHtml("#388B70")),
            new BiomeGroup("Tundra", 132f, 924f, ColorTranslator.FromHtml("#0287AA")),
        };
        private Biome[] primaryBiomes =
        {
            //new Biome("forest", 0.0f, 0.0f, new string[0], new float[0], ColorTranslator.FromHtml("#009900"), Biome.BiomeType.primary, 0.9f),
            //new Biome("desert", 1.0f, -1.0f, new string[0], new float[0], ColorTranslator.FromHtml("#FFCC33"), Biome.BiomeType.primary, 0.9f),
            //new Biome("moutain", -0.2f, -0.3f, new string[0], new float[0], ColorTranslator.FromHtml("#333300"), Biome.BiomeType.primary, 0.9f),
            //new Biome("ocean", 0.1f, 0.8f, new string[0], new float[0], ColorTranslator.FromHtml("#3333FF"), Biome.BiomeType.primary, 0.9f),
            //new Biome("winter", -1.0f, 0.0f, new string[0], new float[0], ColorTranslator.FromHtml("#39661d"), Biome.BiomeType.primary, 0.9f),
            //new Biome("meadow", -0.5f, -0.7f, new string[0], new float[0], ColorTranslator.FromHtml("#90B494"), Biome.BiomeType.primary, 0.9f),
            //new Biome("swamp", -0.5f, -0.7f, new string[0], new float[0], ColorTranslator.FromHtml("#455624"), Biome.BiomeType.primary, 0.9f),
            //new Biome("rock", -0.5f, -0.7f, new string[0], new float[0], ColorTranslator.FromHtml("#d3d3c2"), Biome.BiomeType.primary, 0.9f),
            //new Biome("autumn", -0.5f, -0.7f, new string[0], new float[0], ColorTranslator.FromHtml("#d39a08"), Biome.BiomeType.primary, 0.9f),
            //new Biome("rainforest", -0.5f, -0.7f, new string[0], new float[0], ColorTranslator.FromHtml("#44890b"), Biome.BiomeType.primary, 0.9f),
            //new Biome("redrock", -0.5f, -0.7f, new string[0], new float[0], ColorTranslator.FromHtml("#f28f52"), Biome.BiomeType.primary, 0.9f),
            new Biome("Rainforest", 865f, 285f, new string[0], new float[0], ColorTranslator.FromHtml("#014A2B"), Biome.BiomeType.primary, 0.9f),
            new Biome("Savanna", 870f, 639f, new string[0], new float[0], ColorTranslator.FromHtml("#6BA127"), Biome.BiomeType.primary, 0.9f),
            new Biome("Desert", 862f, 917f, new string[0], new float[0], ColorTranslator.FromHtml("#905032"), Biome.BiomeType.primary, 0.9f),
            new Biome("Grassland", 509f, 946f, new string[0], new float[0], ColorTranslator.FromHtml("#C1A700"), Biome.BiomeType.primary, 0.9f),
            new Biome("SparseWoods", 616f, 816f, new string[0], new float[0], ColorTranslator.FromHtml("#982B26"), Biome.BiomeType.primary, 0.9f),
            new Biome("Forest", 582f, 627f, new string[0], new float[0], ColorTranslator.FromHtml("#00466A"), Biome.BiomeType.primary, 0.9f),
            new Biome("TemperateRainforest", 648f, 422f, new string[0], new float[0], ColorTranslator.FromHtml("#2A6682"), Biome.BiomeType.primary, 0.9f),
            new Biome("BorealForest", 311f, 780f, new string[0], new float[0], ColorTranslator.FromHtml("#388B70"), Biome.BiomeType.primary, 0.9f),
            new Biome("Tundra", 132f, 924f, new string[0], new float[0], ColorTranslator.FromHtml("#0287AA"), Biome.BiomeType.primary, 0.9f),
            new Biome("DenseForest", 809f, 403f, new string[0], new float[0], ColorTranslator.FromHtml("#014A2B"), Biome.BiomeType.primary, 0.9f),
            new Biome("Fields", 780f, 814f, new string[0], new float[0], ColorTranslator.FromHtml("#6BA127"), Biome.BiomeType.primary, 0.9f),
            new Biome("DarkWoods", 631f, 781f, new string[0], new float[0], ColorTranslator.FromHtml("#982B26"), Biome.BiomeType.primary, 0.9f),
            new Biome("Dunes", 952f, 844f, new string[0], new float[0], ColorTranslator.FromHtml("#905032"), Biome.BiomeType.primary, 0.9f),
            new Biome("Arctic", 42f, 961f, new string[0], new float[0], ColorTranslator.FromHtml("#0287AA"), Biome.BiomeType.primary, 0.9f),
            new Biome("SparseBoreal", 205f, 838f, new string[0], new float[0], ColorTranslator.FromHtml("#0287AA"), Biome.BiomeType.primary, 0.9f),
            new Biome("NorthernRainforest", 388f, 599f, new string[0], new float[0], ColorTranslator.FromHtml("#388B70"), Biome.BiomeType.primary, 0.9f),
            new Biome("Swamp", 653f, 515f, new string[0], new float[0], ColorTranslator.FromHtml("#00466A"), Biome.BiomeType.primary, 0.9f),
        };

        private Biome[] subBiomes =
        {
            new Biome("SubTest", 2.0f, 0.2f, new string[0], new float[0], ColorTranslator.FromHtml("#000000"), Biome.BiomeType.sub, 0.5f),
        };
        private int mapwidth = 1700;
        private int mapheight = 1700;
        private int cellSize = 10;
        private int biomeCellSize = 100;
        private string seedString = "45645456";
        //rule of thumb set this equal to the cellSize because otherwise square tiles will happen
        private float pointRandomness = 15;
        private int index;
        private bool working;

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            mapwidth = pictureBox1.Width;
            mapheight = pictureBox1.Height;
            cellSize = int.Parse(textBox_cellSize.Text);
            biomeCellSize = int.Parse(textBox_biomeCellSize.Text);
            seedString = textBox_SeedString.Text;
            if (checkBox_seedString.Checked)
                seedString = int.Parse(seedString.GetHashCode().ToString()) + 1 + "";
            pointRandomness = int.Parse(textBox_PointRandomness.Text);
            textBox_cellSize.Text = cellSize.ToString();
            textBox_biomeCellSize.Text = biomeCellSize.ToString();
            textBox_SeedString.Text = seedString;
            textBox_PointRandomness.Text = pointRandomness.ToString();
            if (working) return;
            richTextBox_log.Text = "";
            //displayPoints();
            seedString += 1;
            BuildBiomes buildBiomes = new BuildBiomes();
            BuildVoronoi buildVoronoi = new BuildVoronoi();
            CellsToBitmap cellsToBitmap = new CellsToBitmap();
            Cell[] cells = buildBiomes.generate(buildVoronoi.generate(mapwidth, mapheight, cellSize, seedString, pointRandomness), primaryBiomes, primaryBiomeGroups, mapwidth, mapheight, biomeCellSize, seedString, 8, cellSize);
            pictureBox1.Image = cellsToBitmap.generate(cells, mapwidth, mapheight, cellSize);
            Task t = Task.Factory.StartNew(() =>
            {
                Thread.Sleep(50);
                working = false;
            });
        }

        public void displayPoints()
        {
            working = true;
            richTextBox_log.Clear();
            index++;
            BuildVoronoi b = new BuildVoronoi();
            //make it a good rule that pointRandomness should not exceed cell size
            Cell[] cells = b.generate(pictureBox1.Width, pictureBox1.Height, 20, "seed" + index, 15);
            pictureBox1.Image = b.distributPointsToBitmap(pictureBox1.Width, pictureBox1.Height, b.global_vertices);
        }

        public static void log(string s)
        {
            frm.Invoke(new Action(() =>
            {
                if (frm.richTextBox_log.Lines.Length > 47)
                    frm.richTextBox_log.Lines = SubArray(frm.richTextBox_log.Lines, 1, frm.richTextBox_log.Lines.Length - 1);
                frm.richTextBox_log.Text += s + "\n";
            }));
            
        }
        public static T[] SubArray<T>(T[] data, int index, int length)
        {
            T[] result = new T[length];
            Array.Copy(data, index, result, 0, length);
            return result;
        }
    }
}
